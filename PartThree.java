import java.util.Scanner;
public class PartThree{
	
		public static void main(String[]args){
			
			Scanner scan = new Scanner(System.in);
			Calculator cal = new Calculator();
			
			
			System.out.println("Enter the first value");
			double val1= scan.nextDouble();
			System.out.println("Enter the second value");
			double val2= scan.nextDouble();
			
			//Addition
			System.out.println("This is the addition "+ Calculator.add(val1,val2));
			//Substraction
			System.out.println("This is the substraction "+ Calculator.substract(val1,val2));
			//Multiplication
			System.out.println("This is the multiplication "+ cal.multiply(val1,val2));
			//Division
			System.out.println("This is the division "+ cal.divide(val1,val2));
}
}
