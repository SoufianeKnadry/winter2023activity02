public class MethodsTest {
    public static void main(String[]args){
        int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		methodTwoInputNoReturn(5,10);
		
		int z = methodNoInputReturnInt();
		System.out.println();
		System.out.println("This is the value that I returned from the method "+ z);
		System.out.println();
		System.out.println(sumSquareRoot(9,5));
		
		String s1="java";
		String s2="programming";
		System.out.println("length of s1 "+ s1.length()+" length of s2 "+ s2.length()); 
		System.out.println();
		
		SecondClass sc = new SecondClass();
		System.out.println("This is result of addOne "+SecondClass.addOne(50));
		System.out.println("This is result of addTwo "+sc.addTwo(50));
		
	}
    public static void methodNoInputNoReturn(){
        int x = 20;
		System.out.println("I'm in a method that takes no input and returns nothing");
        System.out.println(x);
    }
	public static void methodOneInputNoReturn(int a){
		System.out.println("Inside the method one input no return");
		a -=5;
		System.out.println(a);
	}
	public static void methodTwoInputNoReturn(int a, double b){
		System.out.println("This is the method that takes as input two values");
		System.out.println("This is the int "+a+" This is the double "+b);
	
	}
	public static int methodNoInputReturnInt(){
		
		return 5;
	}
	public static double sumSquareRoot(int a, int b){
		System.out.println("This is the method that returns the square root");
		int c = a+b;
		return Math.sqrt(c);
	}
}
